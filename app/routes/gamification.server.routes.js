var levels = require('../../app/controllers/gamification.server.controller'),
    ranks = require('../../app/controllers/ranks.server.controller');

module.exports = function(app) {
    app.route('/api/levels/:levelName')
        .get(levels.read);

    app.param('levelName', levels.levelByName);
    
    app.route('/api/ranks/')
        .get(ranks.list);
        
    app.route('/api/ranks/:rankValue')
    .get(ranks.read);

    app.param('rankValue', ranks.ranksByValue);
};
