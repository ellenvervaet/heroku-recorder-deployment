var users = require('../../app/controllers/users.server.controller'),
    studies = require('../../app/controllers/studies.server.controller');

module.exports = function(app) {
    
    app.route('/api/studies/')
        .get(studies.list)
        //je moet ingelogd zijn om een note te maken
        .post(users.requiresLogin, studies.create);

    app.route('/api/studies/:studyId')
        .get(studies.read)
        .delete(/*users.requiresLogin, studies.hasAuthorization,*/ studies.delete);

    app.param('studyId', studies.studyByID);
    
    app.route('/api/studies-rank/:rank')
        .get(studies.read);

    app.param('rank', studies.studiesByRank);
};
