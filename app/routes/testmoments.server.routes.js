var testmoments = require('../../app/controllers/testmoments.server.controller');

module.exports = function(app) {
    
    app.route('/api/testmoments/')
        .get(testmoments.list)
        //je moet ingelogd zijn om een note te maken
        .post(testmoments.create);

    app.route('/api/testmoments/:testmomentId')
        .get(testmoments.read)
        .delete(/*users.requiresLogin, testmoments.hasAuthorization,*/ testmoments.delete);

    app.param('testmomentId', testmoments.testmomentByID);
   
};
