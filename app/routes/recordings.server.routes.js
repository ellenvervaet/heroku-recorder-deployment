var users = require('../../app/controllers/users.server.controller'),
    recordings = require('../../app/controllers/recordings.server.controller');

module.exports = function(app) {
    
    app.route('/api/recordings/')
        .get(recordings.list)
        .put(recordings.update)
        .post(recordings.create);
        
    app.route('/api/feedback-recordings')
        .get(recordings.listByFeedback);

    app.route('/api/recordings/:recordingId')
        .get(recordings.read)
        .put(recordings.update)
        .delete( recordings.delete);

    app.param('recordingId', recordings.recordingByID);
    
    app.route('/api/recordings-user/:userId')
        .get(recordings.read);

    app.param('userId', recordings.recordingsByUserId);
    
    app.route('/api/testRecordings/:userId2')
        .get(recordings.read);

    app.param('userId2', recordings.testRecordingsByUserId);
};