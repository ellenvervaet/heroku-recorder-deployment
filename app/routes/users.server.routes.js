var users = require('../../app/controllers/users.server.controller'),
    passport = require('passport');

module.exports = function(app) {
    
    //dit verwijst naar de functies in users.server.controller.js
    app.route('/api/users')
        .post(users.create)
        .get(users.list);
        
    app.route('/api/users/:userId')
        .get(users.read)
        .put(users.update)
        .delete(users.delete);
    app.param('userId', users.userByID);
    
    app.route('/api/usersByName/:userName')
        .get(users.read)
        .put(users.update);
    app.param('userName', users.userByUsername);
    
    app.route('/register')
        .get(users.renderRegister)
        .post(users.register);
        
    app.route('/login')
        .get(users.renderLogin);
    //post zit in index
        
    app.get('/logout', users.logout);
}