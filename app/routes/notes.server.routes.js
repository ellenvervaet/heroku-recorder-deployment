var users = require('../../app/controllers/users.server.controller'),
    notes = require('../../app/controllers/notes.server.controller');

module.exports = function(app) {
    
    app.route('/api/notes/')
        .get(notes.list)
        //je moet ingelogd zijn om een note te maken
        .post(users.requiresLogin, notes.create);

    app.route('/api/notes/:noteId')
        .get(notes.read)
        //het moet jouw note zijn om dit te kunnenn doen
        .put(/*users.requiresLogin, notes.hasAuthorization, */notes.update)
        .delete(/*users.requiresLogin, notes.hasAuthorization,*/ notes.delete);

    app.param('noteId', notes.noteByID);
    
    app.route('/api/notes-rank/:rank')
        .get(notes.read);

    app.param('rank', notes.notesByRank);
};
