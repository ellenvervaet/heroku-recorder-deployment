var Testmoment = require('mongoose').model('Testmoment');
    
//CRUD
//=================================
exports.create = function(req, res, next) {
    var testmoment = new Testmoment(req.body);
    testmoment.save(function(err) {
        if (err) {
            return next(err);
        }
        else {
            res.json(testmoment);
        }
    })
}

exports.list = function(req, res, next) {
    Testmoment.find({}, function(err, testmoments) {
        if (err) {
            return next(err);
        }
        else {
            res.json(testmoments);
        }
    });
};
//responds with a json representation of the req.testmoment object
exports.read = function(req, res) {
    res.json(req.testmoment);
};

exports.testmomentByID = function(req, res, next, id) {
    Testmoment.findOne({
        _id: id
    },
    function(err, testmoment) {
        if (err) {
            return next(err);
        }
        else{
            
            req.testmoment = testmoment;
            next();
        }
    });
};


exports.testmomentsByUserId = function(req, res, next, value) {
  Testmoment.find({
        user: value 
    },
    function(err, testmoment) {
        if (err) {
            return next(err);
        }
        else{
            req.testmoment = testmoment;
            next();
        }
    }).sort(
        {
            date : -1
        });
};

exports.update = function(req, res, next) {
    Testmoment.findByIdAndUpdate(req.body._id, req.body, {new: true}, function(err, testmoment) {
        if (err) {
            return next(err);
        }
        else {
            res.json(testmoment);
        }
    })
};

exports.delete = function(req, res, next) {
    req.testmoment.remove(function(err) {
        
        if (err) {
            return next(err);
        }
        else {
            res.json(req.testmoment);
        }
        
    })
};