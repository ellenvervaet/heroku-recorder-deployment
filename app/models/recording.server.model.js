var mongoose = require('mongoose'),
    crypto = require('crypto'), 
    Schema = mongoose.Schema;
    
var RecordingSchema = new Schema({
    
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'            
        },
        username: String,
        created_at: {
            type: Date,
            default: Date.now
        },
        deleted_at: Date,
        rank: Number,
        title: String,
        author: String,
        description: String,
        data : String,
        test: Boolean,
        testChecklist: {
            tempo : Number,
            notes : Number,
            rythm : Number,
            musicality : Number
        },
        inFeedback: {
            type: Boolean,
            default: false
        },
        approvedFeedbacks:{
            type: Number,
            default: 0
        },
        feedbacks: [{
            created_at: {
                type: Date,
                default: Date.now
            },
            username: String,
            deleted_at: Date,
            body: String,
            approved: {
                type: Boolean,
                default: false
            },
            likes: [{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            }]
        }]
});

mongoose.model('Recording', RecordingSchema);