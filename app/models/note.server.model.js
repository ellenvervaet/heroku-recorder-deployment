var mongoose = require('mongoose'),
    crypto = require('crypto'), 
    Schema = mongoose.Schema;
    
var NoteSchema = new Schema({
    
        rank: Number,
        name: String,
        octave: Number,
        key: String,
        points: Number,
        finger: String,
        treble: Boolean,
        tips: {}
    
});

mongoose.model('Note', NoteSchema);