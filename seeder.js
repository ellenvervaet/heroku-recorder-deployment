var seeder = require('mongoose-seeder'),
    
    //MODELS
    User = require('./app/models/user.server.model'),
    Level = require('./app/models/gamification.server.model'),
    Note = require('./app/models/note.server.model'),
    Study = require('./app/models/study.server.model'),
    Recording = require('./app/models/recording.server.model'),
    
    data = require('./seeder-data.json'),
    mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/recorder');

mongoose.connection.on('connected', function() {
    console.log('connected');
    seeder.seed(data, {dropDatabase: true}).then(function(dbData) {
        console.log('database seeded');
    }).catch(function(err) {
        console.log(err);
    });
});

mongoose.connection.on('error',function (err) {  
  console.log('Mongoose default connection error: ' + err);
}); 

