;(function() {
    'use strict';
    
    angular.module('app.notes')
    
    .controller('PickANoteController', PickANoteController);
    
    //PickANoteController.$inject['$scope', '$routeParams', '$location', 'Notes', '$rootScope'];
    
    function PickANoteController($scope, $routeParams, $location, Notes, $rootScope, RankedNotes, NoteService, Pitcher) {
        //laadt het menu
        $scope.user = $rootScope.user;
        $scope.menuTemplate = "templates/partials/menu.html";

        //hover om de noot op de notenbalk te zien
        $scope.showNote = function(note, field){
                $(field).prev().hide();
                Pitcher.fillExampleField(note, field);
            } 
        $scope.clearField = function(canvas){
            $('#' + canvas).prev().show();
            canvas = document.getElementById(canvas);
            var context = canvas.getContext("2d");
            context.clearRect(0, 0, canvas.width, canvas.height);
        }
        //heb je de noot al geleerd of niet?
        $scope.checkNote = function(note){
            return NoteService.CheckIfNoteLearned(note, $scope.user);
        }
        
        //CRUD
        var rank = $scope.user.rank.value + 1;
        $scope.find = function() {
            $scope.notes = RankedNotes.query({
                rank: rank
            });
        };
        
        
    }
    
})();
