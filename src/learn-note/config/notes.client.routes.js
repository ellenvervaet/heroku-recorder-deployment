;(function() {
    'use strict';
    angular.module('app.studies')
    .config(Routes);
    
    //Routes.$inject['$routeProvider'];
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/pick-note', {
            templateUrl: 'templates/learn-note/views/pick-note.client.view.html',
            controller: 'PickANoteController'
        }).
        when('/learn-note/:noteId', {
            templateUrl: 'templates/learn-note/views/learn-note.client.view.html',
            controller: 'LearnANoteController'
        });
    }
})();
