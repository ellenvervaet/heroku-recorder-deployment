;(function() {
    'use strict';
    angular.module('app.notes')
    .factory('Pitcher', Pitcher);
    
    function Pitcher($q, ngDialog){
        var audioContext = new AudioContext(),
            MAX_SIZE = Math.max(4,Math.floor(audioContext.sampleRate/5000)), // corresponds to a 5kHz signal
            isPlaying = false,
            analyser = null, 
            mediaStreamSource, //Moet hier, want anders komt het te vroeg in de garbage collector waardoor het stopt met luisteren
            pitch,
            noteToLearn,
            publicNote = "Do", //de noot die gespeeld wordt
            myVar, //timer
            ac, //is er een noot?
            octaaf, //octaaf van de gespeelde noot (alt != sopraan)
            buf = new Float32Array( 1024 ),
            countDown = 5;    
            
        var service = {
            noteToLearn: noteToLearnGS,
            myVar: myVarGS,
          
            fillExampleField: fillExampleField,   
            gotStream: gotStream,
            updatePitch: updatePitch,
            meetDoel: meetDoel
        };
        
        return service;
                
        //=============================
        //DECLARE VARIABLES
        //=============================
        window.AudioContext = window.AudioContext || window.webkitAudioContext;

        
        
        function noteToLearnGS(note){
            noteToLearn = note;
        }
        function myVarGS(vari){
            myVar = vari;
        }
        
        //=============================
        //PUBLIC FUNCTIONS
        //=============================
            
        function fillExampleField(note, canvas) {
            var exampleCanvas = $(canvas)[0];
            var exampleRenderer = new Vex.Flow.Renderer(exampleCanvas, Vex.Flow.Renderer.Backends.CANVAS);
            var exampleContext = exampleRenderer.getContext();
            var exampleStave = new Vex.Flow.Stave(10, 0, 150);
            
            exampleStave.addClef("treble").setContext(exampleContext).draw();

            var key = note.key + '/' + note.octave;

            var exampleNote = [
                new Vex.Flow.StaveNote({ keys: [key], duration: "q"})
            ];

            //kruis of mol toevoegen indien nodig
            if(note.key.length == 2){
                var accidental = note.key.substr(note.key.length - 1);
                exampleNote[0].addAccidental(0, new Vex.Flow.Accidental(accidental));
            }
            
            //een maat moet steeds vol zijn, voor 1 kwartnoot dus: 1 beat met value 4(kwartnoot)
            var exampleVoice = new Vex.Flow.Voice({
                num_beats: 1,
                beat_value: 4,
                resolution: Vex.Flow.RESOLUTION
            });

            exampleVoice.addTickables(exampleNote);

            var formatter = new Vex.Flow.Formatter().
                joinVoices([exampleVoice]).format([exampleVoice], 250);

            exampleVoice.draw(exampleContext, exampleStave);
        }

        function gotStream(stream) {
            // Create an AudioNode from the stream.
            mediaStreamSource = audioContext.createMediaStreamSource(stream);

            //The createAnalyser() method of the AudioContext interface creates an AnalyserNode, 
            //which can be used to expose audio time and frequency data and create data visualisations.
            analyser = audioContext.createAnalyser();
            analyser.fftSize = 2048;
            //Connect the two
            mediaStreamSource.connect( analyser );   
        }

        function updatePitch() {
            
            var noteElem = $( "#notePlayed" );

            analyser.getFloatTimeDomainData( buf );
            ac = autoCorrelate( buf, audioContext.sampleRate ); //autoCorrelate filtert ontoevalligheden

            //er is geen noot
            if (ac == -1) {
                noteElem.innerText = "...";
            }//er is een noot
            else {
                //frequentie
                pitch = ac;
                
                //NOOTNAAM IN HTML
                //note is een afgerond getal. deel door 12 om de correcte index van notestrings te bekomen (er zijn nml 12 noten)
                var noteStrings = ["Do", "Do#", "Re", "Mib", "Mi", "Fa", "Fa#", "Sol", "Sol#", "La", "Sib", "Si"];
                var note =  noteFromPitch( pitch ); 
                noteElem.html(noteStrings[note%12].toLocaleLowerCase());
                publicNote = noteElem.innerHTML = noteStrings[note%12];

                setOctave(note); //bereken de var octaaf
                
                drawPlayedNote();
            }

            //zorgt ervoor dat updatePitch telkens opnieuw uitgevoerd wordt en het canvas opnieuw getekend
            if (!window.requestAnimationFrame)
                window.requestAnimationFrame = window.webkitRequestAnimationFrame;
            var rafID = window.requestAnimationFrame( updatePitch );
        }


        function meetDoel(){
            return $q( function(resolve, reject) {
                var htmlBlock = $("#countDown");
                var htmlText = $("#countDown h2");
                htmlText.html(countDown);
                //De juiste noot wordt gespeeld
                if(publicNote.toLowerCase() == noteToLearn.name.toLowerCase() && ac !== -1 && octaaf == noteToLearn.octave){
                    htmlBlock.css("display", "block");
                    if(countDown == 0){
                        ngDialog.open({ template: 'templates/partials/noteLearned.html', className: 'ngdialog-theme-default' });
                        window.clearInterval(myVar);
                        resolve('noot geleerd');
                    }else{
                        htmlText.innerHTML = countDown;
                        countDown = countDown - 1;
                        reject('noot niet lang genoeg aangehouden');
                    }
                // De foute noot wordt gespeeld
                }else{
                    countDown = 5;
                    htmlBlock.css("display", "none");
                    reject('de foute noot gespeeld');
                }
                
            });
        };
        
        //=============================
        //LOCAL FUNCTIONS
        //=============================

        function noteFromPitch( frequency ) {
            var noteNum = 12 * (Math.log( frequency / 440 )/Math.log(2) );
            return Math.round( noteNum ) + 69;
        }

        //100 cent is een halve noot
        //https://nl.wikipedia.org/wiki/Cent_%28muziek%29
        //formule die hier gebruikt wordt is een algemene formule die hierboven te vinden is
        function centsOffFromPitch( frequency, note ) {
            return Math.floor( 1200 * Math.log( frequency / frequencyFromNoteNumber( note ))/Math.log(2) );
        }

        function frequencyFromNoteNumber( note ) {
            return 440 * Math.pow(2,(note-69)/12);
        }

        function setOctave(note) {

            switch(true){
                    case (note>=60 && note<72):
                        octaaf = 3;
                        break;
                    case (note>=72 && note<84):
                        octaaf = 4;
                        break;
                    case (note>=84 && note<96):
                        octaaf = 5;
                        break;
                    case (note>=96 && note<108):
                        octaaf = 6;
                        break;
                }

                //Bij alt wordt ocaaf + 1 want die klinkt lager voor dezelfde noot
                if(noteToLearn.treble){
                    ++octaaf;
                    }
        }

        function drawPlayedNote(){
            
                var exerciseCanvas = $("#exerciseNote")[0];
                var exerciseVoice = new Vex.Flow.Voice({
                    num_beats: 1,
                    beat_value: 4,
                    resolution: Vex.Flow.RESOLUTION
                });
                var exerciseRenderer = new Vex.Flow.Renderer(exerciseCanvas, Vex.Flow.Renderer.Backends.CANVAS);
                var exerciseContext = exerciseRenderer.getContext();
                exerciseContext.clearRect(0, 0, exerciseCanvas.width, exerciseCanvas.height);
                var exerciseStave = new Vex.Flow.Stave(10, 0, 150);
                exerciseStave.addClef("treble").setContext(exerciseContext).draw();
                var noteArray = null;

                noteArray = getVexFlowNote();

                exerciseVoice.addTickables(noteArray);
                var formatter = new Vex.Flow.Formatter().joinVoices([exerciseVoice]).format([exerciseVoice], 250);
                exerciseVoice.draw(exerciseContext, exerciseStave);
                
        }

        function getVexFlowNote(){
            var noteArray;
            
            switch(publicNote){
                    case "Do":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["c/" + octaaf], duration: "q"}) ];
                        break;
                    case "Do#":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["c#/" + octaaf], 
                        duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("#")) ];
                        break;
                    case "Re":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["d/" + octaaf], duration: "q"}) ];
                        break;
                    case "Mib":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["eb/" + octaaf], duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("b")) ];
                        break;
                    case "Mi":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["e/" + octaaf], duration: "q"}) ];
                        break;
                    case "Fa":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["f/" + octaaf], duration: "q"}) ];
                        break;
                    case "Fa#":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["f#/" + octaaf], duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("#")) ];
                        break;
                    case "Sol":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["g/" + octaaf], duration: "q"}) ];
                        break;
                    case "Sol#":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["g#/" + octaaf], duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("#")) ];
                        break;
                    case "La":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["a/" + octaaf], duration: "q"}) ];
                        break;
                    case "Sib":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["bb/" + octaaf], duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("b")) ];
                        break;
                    case "Si":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["b/" + octaaf], duration: "q"}) ];
                        break;
                    default :
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["g/" + octaaf], duration: "q"}) ];
                        break;
                }
                
            return noteArray;
        }

        //toevalligheden
        function autoCorrelate( buf, sampleRate ) {
            var SIZE = buf.length;
            var MAX_SAMPLES = Math.floor(SIZE/2);
            var best_offset = -1;
            var best_correlation = 0;
            var rms = 0;
            var foundGoodCorrelation = false;
            var correlations = new Array(MAX_SAMPLES);
            var MIN_SAMPLES = 0;  // will be initialized when AudioContext is created.

            for (var i=0;i<SIZE;i++) {
                var val = buf[i];
                rms += val*val;
            }
            rms = Math.sqrt(rms/SIZE);
            if (rms<0.01) // not enough signal
                return -1;

            var lastCorrelation=1;
            for (var offset = MIN_SAMPLES; offset < MAX_SAMPLES; offset++) {
                var correlation = 0;

                for (var i=0; i<MAX_SAMPLES; i++) {
                    correlation += Math.abs((buf[i])-(buf[i+offset]));
                }
                correlation = 1 - (correlation/MAX_SAMPLES);
                correlations[offset] = correlation; // store it, for the tweaking we need to do below.
                if ((correlation>0.9) && (correlation > lastCorrelation)) {
                    foundGoodCorrelation = true;
                    if (correlation > best_correlation) {
                        best_correlation = correlation;
                        best_offset = offset;
                    }
                } else if (foundGoodCorrelation) {
                    // short-circuit - we found a good correlation, then a bad one, so we'd just be seeing copies from here.
                    // Now we need to tweak the offset - by interpolating between the values to the left and right of the
                    // best offset, and shifting it a bit.  This is complex, and HACKY in this code (happy to take PRs!) -
                    // we need to do a curve fit on correlations[] around best_offset in order to better determine precise
                    // (anti-aliased) offset.

                    // we know best_offset >=1,
                    // since foundGoodCorrelation cannot go to true until the second pass (offset=1), and
                    // we can't drop into this clause until the following pass (else if).
                    var shift = (correlations[best_offset+1] - correlations[best_offset-1])/correlations[best_offset];
                    return sampleRate/(best_offset+(8*shift));
                }
                lastCorrelation = correlation;
            }
            if (best_correlation > 0.01) {
                // console.log("f = " + sampleRate/best_offset + "Hz (rms: " + rms + " confidence: " + best_correlation + ")")
                return sampleRate/best_offset;
            }
            return -1;
        //	var best_frequency = sampleRate/best_offset;
        }
    }
    
})();