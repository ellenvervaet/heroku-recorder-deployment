;(function() {
    'use strict';
    
    angular.module('app.feedback')
    
    .controller('FeedbackController', FeedbackController);
    
    //PickAStudyController.$inject['$scope', '$routeParams', '$location', 'Authentication'];
     
    function FeedbackController($scope, $location, $rootScope, FeedbackRecordings, Audio, Recordings) {
        //laadt het menu
        $scope.user = $rootScope.user
        $scope.menuTemplate = "templates/partials/menu.html";
                
        $scope.newFeedback = {};
        $scope.newFeedback.body = "";
        
        $('.feedbackRecording-selected').hide();
        $scope.showDetails = function(feedbackrecording){
            //wanneer de gebruiker verschillende opnames aanklikt,
            //verdwijnt de vorige, geen get() dus in scope wijzigen.
            updatePrevRecInScope();
            
            $scope.pickedFeedbackRecording = feedbackrecording;   
            
            //heb je al feedback gegeven? Mag maar 1x
            $scope.givenFeedback = false;
            console.log($scope.pickedFeedbackRecording.feedbacks);

            $scope.pickedFeedbackRecording.feedbacks.forEach(function(feedback){
                console.log(feedback);
                if(feedback.username == $scope.user.username){
                    console.log('returned');
                    $scope.givenFeedback = true;
                }
            });
            
            //indien je geen feedback meer mag geven, verdwijnt form en wordt comments langer
            if($scope.pickedFeedbackRecording.username == $scope.user.username || $scope.givenFeedback == true){
                $('.comments').addClass('long');
            } else{
                $('.comments').removeClass('long');
            }
            
            $('audio').remove();
            
            Audio.placeTag('audio', $scope.pickedFeedbackRecording.data);
               
            $('.feedbackRecording-selected').show();
            $('.feedbackRecording-unselected').hide();
        }
        
        //CRUD
        $scope.find = function() {
            $scope.feedbackRecordings = FeedbackRecordings.query({
            }).$promise.then(function(recording){
                $scope.feedbackRecordings = recording;
                console.log(recording);
            });
        };
        
        $scope.addFeedback = function(){
            $scope.givenFeedback = true;
            $scope.newFeedback.username = $scope.user.username;
            $scope.newFeedback.likes = [];
            $scope.newFeedback.created_at = Date.now();
            $scope.newFeedback.approved = false;
            $scope.pickedFeedbackRecording.feedbacks.push($scope.newFeedback);
            Recordings.get({
                recordingId: $scope.pickedFeedbackRecording._id
            }).$promise.then(function(recording){
                recording.feedbacks.push($scope.newFeedback);
                recording.$update(function(){
                
                }, function(errorResponse) {
                    $scope.error = errorResponse.data.message;
                });
            });  
        }
        
        $scope.like = function(feedbackId){
            Recordings.get({
                recordingId: $scope.pickedFeedbackRecording._id
            }).$promise.then(function(recording){
                
                $scope.pickedFeedbackRecording.feedbacks.forEach(function(feedback) {
                    if(feedback._id == feedbackId){
                        feedback.likes.push($scope.user._id);
                    }
                });
                
                recording.feedbacks.forEach(function(feedback) {
                    if(feedback._id == feedbackId){
                        feedback.likes.push($scope.user._id);
                    }
                });
                recording.$update(function(){
                    
                }, function(errorResponse) {
                    $scope.error = errorResponse.data.message;
                });
            });
        }
        $scope.approve = function(feedbackId){
            Recordings.get({
                recordingId: $scope.pickedFeedbackRecording._id
            }).$promise.then(function(recording){
                $scope.pickedFeedbackRecording.approvedFeedbacks++;
                $scope.pickedFeedbackRecording.feedbacks.forEach(function(feedback) {
                    if(feedback._id == feedbackId){
                        feedback.approved = true;
                    }
                });
                
                recording.approvedFeedbacks++;
                if(recording.approvedFeedbacks >= 3){
                    removeFromFeedbackScope(recording);
                }
                recording.feedbacks.forEach(function(feedback) {
                    if(feedback._id == feedbackId){
                        feedback.approved = true;
                    }
                });
                recording.$update(function(){
                    
                }, function(errorResponse) {
                    $scope.error = errorResponse.data.message;
                });
            });
        }
        
        function updatePrevRecInScope(){
            $scope.feedbackRecordings.forEach(function(recording){
                if($scope.pickedFeedbackRecording){
                    if(recording._id == $scope.pickedFeedbackRecording._id){
                        recording = $scope.pickedFeedbackRecording;
                    }
                }
            })
        }
        
        function removeFromFeedbackScope(currentRecording){
            $('.feedbackRecording-selected').hide();
            $('.feedbackRecording-unselected').show();
            
            var index = 0;
            $scope.feedbackRecordings.forEach(function(recording){
                if(recording._id == currentRecording._id){
                    $scope.feedbackRecordings.splice(index, 1);
                }
                index++;
            })
        }
    }
    
})();
