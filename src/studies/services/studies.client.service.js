;(function() {
    'use strict';
    
    angular.module('app.studies')
    
    .factory('Studies', Studies)
    .factory('RankedStudies', RankedStudies);
    
    //Studies.$inject['$resource'];
    
    function Studies($resource) {
        return $resource('api/studies/:studyId', {
            studyId: '@_id'
        });
    }
    
    function RankedStudies($resource) {
        return $resource('api/studies-rank/:rank', {
            rank: '@rank'
        });
    }
    
})();
