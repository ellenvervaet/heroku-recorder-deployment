;(function() {
    'use strict';
    
    angular.module('app.recordings')
    
    .controller('RecordingsController', RecordingsController);
    
    //PickAStudyController.$inject['$scope', '$routeParams', '$location', 'Authentication'];
     
    function RecordingsController($scope, $location, $rootScope, RecordingsUser, Recordings, $routeParams, Audio) {
        //laadt het menu
        $scope.user = $rootScope.user
        $scope.menuTemplate = "templates/partials/menu.html";
        
            
        
        $scope.goToRecording = function ( id ) {
            $location.path( 'recordings/' + id );
        };
        
        $('.study-selected').hide();
        $scope.showDetails = function(recording){
            $scope.pickedRecording = recording;
            $scope.pickedRecording.numberFeedbacks = Object.keys(recording.feedbacks).length;
            console.log(  $scope.pickedRecording.numberFeedbacks);
            $('audio').remove();
            
            Audio.placeTag('audio', $scope.pickedRecording.data);
            
            $('.study-selected').show();
            $('.study-unselected').hide();
        }
        
        //CRUD
        $scope.find = function() {
            $scope.recordings = RecordingsUser.query({
                userId: $scope.user._id
            }).$promise.then(function(recordings){
                $scope.recordings = recordings;
            });
        };
        $scope.placeInFeedback = function() {
            Recordings.get({
                recordingId: $scope.pickedRecording._id
            }).$promise.then(function(recording){
                recording.inFeedback = true;
                recording.$update(function(){
                    $scope.pickedRecording.inFeedback = true;
                });

            });
        }
    }
    
})();
