;(function() {
    'use strict';
    angular.module('app.notes')
    .factory('Audio', Audio);
    
    function Audio($sce){
       
            
        var service = {
            placeTag: placeTag
        };
        
        return service;
       
       function placeTag(elem, audioSrc){
            var audioElem = document.createElement("audio");
            audioElem.setAttribute("controls", "controls");
            var src = document.createElement("source");
            src.setAttribute('src', trustAudio(audioSrc));
            src.setAttribute('type', 'audio/ogg');
            audioElem.appendChild(src);
            var element = document.getElementById(elem);
            element.appendChild(audioElem); 
       }
       
        function trustAudio(audio) {
            return $sce.trustAsResourceUrl(audio);
        }
    }
    
})();